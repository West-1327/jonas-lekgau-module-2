void main(List<String> args) {
  var appwinner = new Winner();
  appwinner.appname = "Fnb";
  appwinner.category = "Best overall winner";
  appwinner.developer = "Fnb banking team";
  appwinner.year = 2012;

  print(appwinner.appname);
  print(appwinner.category);
  print(appwinner.developer);
  print(appwinner.year);
  Winner.toUpper(appwinner.appname);
}

class Winner {
  String? appname;
  String? category;
  String? developer;
  int? year;

  static toUpper(var name) {
    print(name.toUpperCase());
  }
}
